import { combineReducers } from 'redux'
import contacts from './components/reducers/contacts'
import popup from './components/reducers/popup'

export default combineReducers({
  contacts,
  popup
})
