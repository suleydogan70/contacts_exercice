import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Avatar } from '@material-ui/core'
import * as actions from '../actions'

class Contact extends Component {
  constructor(props) {
    super(props)

    const { contact } = this.props

    this.state = {
      modify: false,
      nom: contact.nom,
      prenom: contact.prenom,
      telephone: contact.telephone
    }

    this.deleteContact = this.deleteContact.bind(this)
    this.modifyContact = this.modifyContact.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.submitContact = this.submitContact.bind(this)
    this.cancelForm = this.cancelForm.bind(this)
  }

  deleteContact() {
    const { contact, deleteContact } = this.props
    deleteContact(contact.nom)
  }

  modifyContact() {
    this.setState({ modify: true })
  }

  handleChange(e) {
    const { id, value } = e.target
    this.setState({ [id]: value })
  }

  submitContact() {
    const { contact, updateContact } = this.props
    const { nom, prenom, telephone } = this.state
    updateContact(contact.nom, { nom, prenom, telephone })
    this.cancelForm()
  }

  cancelForm() {
    this.setState({ modify: false })
  }

  render() {
    const {
      modify,
      nom,
      prenom,
      telephone
    } = this.state
    const { contact } = this.props
    return (
      <li>
        <Avatar src={contact.photo} />
        {
          modify
            ? (
              <div>
                <input id="nom" onChange={this.handleChange} value={nom} />
                <input id="prenom" onChange={this.handleChange} value={prenom} />
                <input id="telephone" onChange={this.handleChange} value={telephone} />
              </div>
            )
            : (
              <div>
                <p>{`${contact.nom} ${contact.prenom}`}</p>
                <p>{contact.telephone}</p>
              </div>
            )
        }
        <div className="actions center">
          {
            modify
              ? (
                <button onClick={this.submitContact} className="btn btn-add" type="button">
                  V
                </button>
              )
              : (
                <button onClick={this.modifyContact} className="btn btn-add" type="button">
                  <img src="src/assets/edit.png" alt="Modifier l'utilisateur" />
                </button>
              )
          }
          <button onClick={modify ? this.cancelForm : this.deleteContact} className="btn btn-cancel" type="button">X</button>
        </div>
      </li>
    )
  }
}

export default connect(null, actions)(Contact)
