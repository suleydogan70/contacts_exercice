import actionsType from '../../actions/types'

/*
 *  Toggle Popup
 *  Open a popup and tell the main element to filter content
 */
export const togglePopup = data => ({
  type: actionsType.TOGGLE_POPUP,
  data
})

/*
 *  Add Contact
 *  Tells the store a new contact is comming
 */
export const addContact = data => ({
  type: actionsType.ADD_CONTACT,
  data
})

/*
 *  Update Contact
 *  Change contact informations (firstname, lastname or phone)
 */
export const updateContact = (nom, data) => ({
  type: actionsType.UPDATE_CONTACT,
  data: { nom, updates: data }
})

/*
 *  Delete Contact
 *  Delete a contact from the store
 */
export const deleteContact = data => ({
  type: actionsType.DELETE_CONTACT,
  data
})
