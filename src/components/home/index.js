import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'

import Contact from './components'

class Home extends Component {
  constructor(props) {
    super(props)

    const random = (Math.floor(Math.random() * 10) + 3) * 100

    this.state = {
      nom: '',
      prenom: '',
      telephone: '',
      photo: `https://source.unsplash.com/random/${random}x${random}`,
      error: false,
      query: ''
    }

    this.showPopup = this.showPopup.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.search = this.search.bind(this)
  }

  showPopup() {
    const { popup, togglePopup } = this.props
    togglePopup(!popup)
  }

  handleSubmit(e) {
    const {
      nom,
      prenom,
      telephone,
      photo
    } = this.state
    const { popup, togglePopup, addContact } = this.props

    e.preventDefault()

    if (nom === '' || prenom === '' || telephone === '') {
      this.setState({ error: true })
    } else {
      addContact({
        nom,
        prenom,
        telephone,
        photo
      })
      togglePopup(!popup)
      this.setState({ error: false })
    }
  }

  handleChange(e) {
    const { id, value } = e.target
    this.setState({ [id]: value })
  }

  search(e) {
    const { value } = e.target
    this.setState({ query: value.toLowerCase() })
  }

  render() {
    const { contacts, popup } = this.props
    const { error, query } = this.state
    contacts.sort((a, b) => (a.nom.toLowerCase() < b.nom.toLowerCase() ? -1 : 1))
    return (
      <div>
        <div className="flex jc-b">
          <h1>Liste des contacts</h1>
          <button onClick={this.showPopup} className="btn btn-add" type="button">Ajouter un contact</button>
        </div>
        <hr />
        <input onChange={this.search} className="search" placeholder="Rechercher..." />
        <ul>
          { query === ''
            ? contacts.map(contact => <Contact key={contact.nom} contact={contact} />)
            : contacts.filter(contact => contact.nom.toLowerCase().indexOf(query) !== -1
            || contact.prenom.toLowerCase().indexOf(query) !== -1
            || contact.telephone.toLowerCase().indexOf(query) !== -1)
              .map(contact => <Contact key={contact.nom} contact={contact} />)
          }
        </ul>
        { popup
          ? (
            <div className="popup">
              <div className="flex jc-b">
                <h2>Ajouter un contact</h2>
                <button onClick={this.showPopup} className="btn btn-cancel" type="button">X</button>
              </div>
              <hr />
              { error ? <p className="alert alert-error">Veuillez fournir tous les champs</p> : '' }
              <form onSubmit={this.handleSubmit}>
                <label htmlFor="nom">
                  <input onChange={this.handleChange} placeholder="nom" id="nom" type="text" value={this.nom} />
                </label>
                <label htmlFor="prenom">
                  <input onChange={this.handleChange} placeholder="prenom" id="prenom" type="text" value={this.prenom} />
                </label>
                <label htmlFor="telephone">
                  <input onChange={this.handleChange} placeholder="telephone" id="telephone" type="text" value={this.telephone} />
                </label>
                <input type="hidden" value="lol" />
                <button type="submit" className="btn btn-add">Ajouter</button>
              </form>
            </div>
          ) : '' }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  contacts: state.contacts.contacts,
  popup: state.popup.popup
})

export default connect(mapStateToProps, actions)(Home)
