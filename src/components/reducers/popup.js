import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  popup: false
}

const togglePopup = (state, action) => (
  fromJS(state)
    .setIn(['popup'], action.data)
    .toJS()
)

const popup = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.TOGGLE_POPUP:
      return togglePopup(state, action)
    default:
      return state
  }
}

export default popup
