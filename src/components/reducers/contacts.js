import MyImmutable from '../../MyImmutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  contacts: [{
    nom: 'Dogan',
    prenom: 'Suleyman',
    telephone: '0769164181',
    photo: 'https://source.unsplash.com/random/600x600'
  }, {
    nom: 'Dalleau',
    prenom: 'Guillaume',
    telephone: '0687230947',
    photo: 'https://source.unsplash.com/random/500x500'
  }]
}

const addContact = (state, action) => MyImmutable.fromJS(state).set(
  'contacts',
  MyImmutable.fromJS(state).get('contacts').push(action.data).toJS()
).toJS()

const updateContact = (state, action) => MyImmutable.fromJS(state).set(
  'contacts',
  MyImmutable.fromJS(state.contacts).update(
    MyImmutable
      .fromJS(state.contacts)
      .findIndex(contact => contact.nom === action.data.nom),
    contact => MyImmutable.fromJS(contact)
      .set('nom', action.data.updates.nom)
      .set('prenom', action.data.updates.prenom)
      .set('telephone', action.data.updates.telephone)
      .toJS()
  )
).toJS()

const deleteContact = (state, action) => MyImmutable.fromJS(state)
  .set('contacts', MyImmutable.fromJS(state.contacts).filter(contact => contact.nom !== action.data))
  .toJS()

const contacts = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.ADD_CONTACT:
      return addContact(state, action)
    case actionsType.DELETE_CONTACT:
      return deleteContact(state, action)
    case actionsType.UPDATE_CONTACT:
      return updateContact(state, action)
    default:
      return state
  }
}

export default contacts
