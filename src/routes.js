import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom'

import * as actions from './components/home/actions'
import Header from './components/header'
import Home from './components/home'
import Footer from './components/footer'

class Routes extends Component {
  constructor(props) {
    super(props)

    this.togglePopup = this.togglePopup.bind(this)
  }

  togglePopup() {
    const { popup, togglePopup } = this.props
    togglePopup(!popup)
  }

  render() {
    const { popup } = this.props
    return (
      <BrowserRouter>
        <div>
          { popup ? <div className="popupFilter" onClick={this.togglePopup} /> : '' }
          <Header />
          <main>
            <Switch>
              <Route path="/" component={Home} exact />
            </Switch>
          </main>
          <Footer />
        </div>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = state => ({
  popup: state.popup.popup
})

export default connect(mapStateToProps, actions)(Routes)
