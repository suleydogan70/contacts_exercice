const MyImmutable = {
  fromJS(data) {
    if (Array.isArray(data)) {
      return this.List(data)
    }
    return this.Map(data)
  },

  Map(data) {
    const freezedData = Object.freeze({ ...data })

    const get = index => this.fromJS(freezedData[index])
    const set = (index, value) => {
      if (typeof value === 'function') {
        value(get(index).toJS())
      }
      return this.fromJS({ ...freezedData, [index]: value })
    }
    const toJS = () => ({ ...freezedData })
    return {
      get,
      set,
      toJS
    }
  },

  List(array) {
    const duplicatedArray = [...array]
    const push = element => this.fromJS(duplicatedArray.concat(element))
    const findIndex = cb => duplicatedArray.findIndex(cb)
    const filter = cb => duplicatedArray.filter(cb)
    const update = (index, cb) => {
      const arrayToUpdate = [...duplicatedArray]
      arrayToUpdate[index] = cb(arrayToUpdate[index])
      return arrayToUpdate
    }
    const toJS = () => [...duplicatedArray]

    return {
      push,
      findIndex,
      filter,
      update,
      toJS
    }
  }
}

export default MyImmutable
